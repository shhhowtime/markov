### Домашние задания для неталогии

[Ссылка на скрипты для домашнего задания по bash](https://github.com/shhhowtime/devops-netology-markov/tree/main/04-script-01-bash "Ссылка на скрипты для домашнего задания по bash")

[Ссылка на скрипты для домашнего задания по Python](https://github.com/shhhowtime/devops-netology-markov/tree/main/04-script-02-py "Ссылка на скрипты для домашнего задания по python")
